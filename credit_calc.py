import math
import argparse

def count_months(pv, m, i):
    n = math.ceil(math.log(m / (m - i * pv), 1 + i))
    return n


def nominal_interest(interest):
    return (interest * 0.01 / 12)


def monthly_payments(pv, m, i):
    i = nominal_interest(float(i))
    n = count_months(pv, m, i)
    overpay = n * m - pv
    years = divmod(n, 12)[0]
    months = divmod(n, 12)[1]
    if years == 0:
        if months == 1:
            return "It will take 1 month to repay this loan!\n" + "Overpayment = " + str(overpay)
        elif months > 1:
            return "It will take " + str(months) + " months to repay this loan!\n" + "Overpayment = " + str(overpay)
    if years == 1:
        if months == 0:
            return "It will take 1 year to repay this loan!\n" + "Overpayment = " + str(overpay)
        elif months == 1:
            return "It will take 1 year and 1 month to repay this loan!\n" + "Overpayment = " + overpay
        else:
            return "It will take " + str(years) + " year and " + str(
                months) + " to repay this loan!\n" + "Overpayment = " + str(overpay)
    if years > 1:
        if months == 0:
            return "It will take " + str(years) + " years to repay this loan!\n" + "Overpayment = " + str(overpay)
        elif months == 1:
            return "It will take " + str(years) + " years and 1 month to repay this loan!\n" + "Overpayment = " + str(
                overpay)
        else:
            return "It will take " + str(years) + " years and " + str(
                months) + " to repay this loan!\n" + "Overpayment = " + str(overpay)


def annuity_monthly(pv, n, i):
    i = nominal_interest(float(i))
    a = pv * i * math.pow(1 + i, n) / (math.pow(1 + i, n) - 1)
    return "Your monthly payment = " + str(math.ceil(a)) + "!"


def loan_principal(pmt, n, i):
    i = nominal_interest(float(i))
    p = pmt / i * (1 - (1 / math.pow(1 + i, n)))
    return "Your loan principal = " + str(p) + "!"


def calculate_differentiate(pv, n, i):
    overpay = 0
    i = nominal_interest(i)
    for m in range(1, int(n) + 1):
        diff = math.ceil((pv / n) + i * (pv - (pv * (m - 1) / n)))
        overpay += diff
        print("Month 1: payment is ", diff)
    print("Overpayment = ", overpay - pv)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Foo')
    parser.add_argument('-t', '--type', help='type of calculation', required=True, choices=['annuity', 'diff'])
    parser.add_argument('-P', '--principal', help='The loan principal', required=False)
    parser.add_argument('-p', '--periods', help='The number of periods', required=False)
    parser.add_argument('-i', '--interest', help='The loan interest', required=False)
    parser.add_argument('-c', '--payment', help='The monthly payment', required=False)
    args = parser.parse_args()

arguments = {'type': args.type, 'principal': args.principal, 'periods': args.periods, 'interest': args.interest,
             'payment': args.payment}
arg_sum = 0
for key, value in arguments.items():
    if value is not None:
        if value[0] == "-":
            print("Incorrect parameters")
            quit()
        arg_sum += 1
if arg_sum < 4 or arg_sum > 4:
    print("Incorrect parameters")
elif args.interest is None:
    print("Incorrect parameters")
elif args.type == 'annuity':
    if args.periods is None:
        pv = float(args.principal)
        m = float(args.payment)
        i = float(args.interest)
        print(monthly_payments(pv, m, i))
    elif args.payment is None:
        a = float(args.principal)
        m = float(args.periods)
        i = float(args.interest)
        print(annuity_monthly(a, m, i))
    elif args.principal is None:
        p = float(args.payment)
        n = float(args.periods)
        i = float(args.interest)
        print(loan_principal(p, n, i))
    else:
        print("Incorrect parameters")
elif args.type == 'diff':
    pv = float(args.principal)
    n = float(args.periods)
    i = float(args.interest)
    calculate_differentiate(pv, n, i)
else:
    print("Incorrect parameters")


# print("""what do you want to calculate?
# type 'n' for number of monthly payments,
# type "a" for annuity monthly payment amount,
# type "p" for loan principal: """)
#
# user = input()
#
# if user == "n":
#     print("Enter the loan principal:")
#     loan_ppal = int(input())
#     print("Enter the monthly payment:")
#     month_pay = int(input())
#     print("Enter the loan interest:")
#     loan_inter = float(input())
#
#     i = (loan_inter / 1200)
#     # print(i)
#
#     no_months = math.log(float(month_pay) / (float(month_pay) - i * float(loan_ppal)), i + 1)
#     no_months = math.ceil(no_months)    # Repr nr de luni in care se plateste rata
#     # print(no_months)
#
#     month = divmod(no_months, 12)
#     # print(month)
#
#     if int(month[0]) > 0 and int(month[1]) > 1:
#         print(f"It will take  years {month[0]} and {month[1]} months to repay this loan!")
#     elif int(month[0]) > 0 and int(month[1]) == 0:
#         print(f"It will take  years {month[0]} to repay this loan!")
#     else:
#         print(f"It will take {month[1]} months to repay this loan!")
#    # y = str(), m = str()
#     # if n > 12:  y = str("s")
#     # if n % 12 != 0: m = str("s")
#     # print(f"It will take {n // 12} year{y} and {n % 12} month{m} to repay this loan!")
#
# elif user == "a":
#     print("Enter the loan principal:")
#     loan_ppal = int(input())
#     print("Enter the number of periods:")
#     no_period = int(input())
#     print("Enter the loan interest:")
#     loan_inter = float(input())
#
#     i = loan_inter / 12 / 100
#     # print(i)
#
#     # annuity = math.log(float(no_period) / (float(no_period) - i * float(loan_ppal)), i + 1)
#     annuity = loan_ppal * ((i * math.pow(1 + i, no_period)) / (math.pow(1 + i, no_period) - 1))
#     annuity = math.ceil(annuity)
#     # print(annuity)
#
#     print(f"Your monthly payment = {annuity}!")
#
# elif user == "p":
#     print("Enter the annuity payment:")
#     annuity = float(input())
#     print("Enter the number of periods:")
#     no_period = int(input())
#     print("Enter the loan interest:")
#     loan_inter = float(input())
#
#     i = loan_inter / 12 / 100
#
#     p = round(annuity / ((i * math.pow(1 + i, no_period)) / (math.pow(1 + i, no_period) - 1)))
#     print(f"Your loan principal = {p}!")
#

#     print('Enter the monthly payment:')
#     month = int(input())
#     month_credit = int(principal_loan / month + 0.5)
#     if month_credit == 1:
#         print(f"It will take {month_credit} month to repay the loan")
#     else:
#         print(f"It will take {month_credit} months to repay the loan")
# elif typ == 'p':
#     print('Enter count of months:')
#     count_of_months = int(input())
#     payment = principal_loan / count_of_months
#     last_payment = principal_loan - (count_of_months - 1) * (math.ceil(payment))
#     if payment == last_payment:
#         print(f'Your monthly payment = {payment}')
#     else:
#         print('Your monthly payment = ' + str(int(math.ceil(payment))) + ' with last month payment = ' + str(last_payment))




